import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import MaterialTable from 'material-table';

import GeneralLayout from 'common/GeneralLayout';
import PageTitle from 'common/PageTitle';
import Loader from 'common/Loader';


import { getSearchResults, deleteSearchResult, updateSearchResult } from 'actions/search';
import getApiErrorMessage from 'utils/getApiErrorMessage';

import columns from './tableColumns';

import {
  CenteredWrapper,
} from './styled';

const Table = () => {


  const dispatch = useDispatch();
  // dispatch on mount redux action which will be picked up by saga
  useEffect(() => {
    dispatch(getSearchResults());
  }, []); //eslint-disable-line


  const { data, error, pending } = useSelector(
    createSelector(
      state => state.search,
      search => ({
        data: search.data,
        error: search.error,
        pending: search.pending,
      })
    )
  );



  if (pending) {
    return (
      <CenteredWrapper>
        <Loader />
      </CenteredWrapper>
    );
  }

  if (error) {
    return (
      <CenteredWrapper>
        <span>{getApiErrorMessage(error)}</span>
      </CenteredWrapper>
    );
  }



  return (
    <GeneralLayout>
      <PageTitle text="Search for users 🤖" />
      <MaterialTable
        title="Data"
        columns={columns}
        data={data}
        editable={{
          onRowUpdate: (newData) =>
            new Promise((resolve) => {
              dispatch(updateSearchResult(newData));
              resolve();
            }),
        }}
        actions={[
          {
            icon: 'delete',
            tooltip: 'Delete User',
            onClick: (_, rowData) => { 
              dispatch(deleteSearchResult(rowData.id));
            },
          },
        ]}
      />
    </GeneralLayout>
  );};

export default Table;

