export default [
  { title: 'Name', field: 'name' },
  { title: 'Gender', field: 'gender', lookup: { female: 'F', male: 'M' } },
  { title: 'Company', field: 'company' },
  { title: 'Active', field: 'isActive', type: 'boolean' },
  { title: 'Age', field: 'age', type: 'numeric' },
  { title: 'Eye color', field: 'eyeColor' },
];
