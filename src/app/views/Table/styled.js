import styled from 'styled-components';

const CenteredWrapper = styled.div`
    display: flex;
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export {
  CenteredWrapper,
};
