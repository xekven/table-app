import React from 'react';
import { Link } from 'react-router-dom';


import GeneralLayout from 'common/GeneralLayout';

import {
  Wrapper,
} from './styled';

const NotFound = () => (
  <GeneralLayout>
    <Wrapper>
      <h1>404. Page you are looking for not found 😖</h1>
      <Link to="/">Go to homepage</Link>
    </Wrapper>
  </GeneralLayout>
);

export default NotFound;

