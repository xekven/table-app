import styled from 'styled-components';

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    /* align-items: stretch; */
    flex: 1;
    justify-content: center;
    align-items: center;
    line-height: 32px;
`;

export {
  Wrapper,
};
