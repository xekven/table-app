import React from 'react';

import { Wrapper } from './styled';

const GeneralLayout = ({ children }) => (
  <Wrapper>
    {children}
  </Wrapper>
);

export default GeneralLayout;
