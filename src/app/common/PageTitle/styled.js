import styled from 'styled-components';

const Element = styled.h1`
    padding: 16px 0;
`;

export { Element };
