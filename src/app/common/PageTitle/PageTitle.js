import React from 'react';
import PropTypes from 'prop-types';

import { Element } from './styled';

const PageTitle = ({ text }) => (
  <Element>{text}</Element>
);

PageTitle.propTypes = {
  text: PropTypes.string,
};


export default PageTitle;


