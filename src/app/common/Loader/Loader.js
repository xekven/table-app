import React from 'react';

import {
  Progress,
} from './styled';

const Loader = () => (
  <Progress />
);

export default Loader;
