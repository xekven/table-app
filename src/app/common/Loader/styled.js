import styled from 'styled-components';

import CircularProgress from '@material-ui/core/CircularProgress';

const Progress = styled(CircularProgress)`

`;

export {
  Progress,
};
