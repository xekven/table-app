import axios from 'axios';

export const getAllSearchResults = () =>
  axios.get('/api/data.json');
