import { takeLatest, all, put } from 'redux-saga/effects';

import { getAllSearchResults } from 'api/searchClient';

import {
  GET_ALL_SEARCH_RESULTS,
  GET_ALL_SEARCH_RESULTS_PENDING,
  GET_ALL_SEARCH_RESULTS_SUCCESS,
  GET_ALL_SEARCH_RESULTS_FAILED,
} from '../static';

function* handleGetSearchResults() {
  yield put({
    type: GET_ALL_SEARCH_RESULTS_PENDING,
  });
  try {
    const results = yield getAllSearchResults();
    yield put({
      type: GET_ALL_SEARCH_RESULTS_SUCCESS,
      payload: results.data,
    });
  } catch (e) {
    yield put({
      type: GET_ALL_SEARCH_RESULTS_FAILED,
      payload: e,
    });
  }
}

export default function* saga() {
  yield all([takeLatest(GET_ALL_SEARCH_RESULTS, handleGetSearchResults)]);
}
