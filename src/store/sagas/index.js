import { all } from 'redux-saga/effects';

import searchSaga from './searchSaga';


export default function* () {
  yield all([
    searchSaga(),
  ]);
}
