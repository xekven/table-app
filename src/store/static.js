
// SEARCH RESULT CONSTANTS
export const GET_ALL_SEARCH_RESULTS = 'GET_ALL_SEARCH_RESULTS';

export const GET_ALL_SEARCH_RESULTS_PENDING = 'GET_ALL_SEARCH_RESULTS_PENDING';
export const GET_ALL_SEARCH_RESULTS_SUCCESS = 'GET_ALL_SEARCH_RESULTS_SUCCESS';
export const GET_ALL_SEARCH_RESULTS_FAILED = 'GET_ALL_SEARCH_RESULTS_FAILED';

export const DELETE_SEARCH_RESULT = 'DELETE_SEARCH_RESULT';
// here should be pending, success, failed events in case of having network call and saga to process that
// because nothing of that exists, item will be just deleted from reducer directly
export const UPDATE_SEARCH_RESULT = 'UPDATE_SEARCH_RESULT';
