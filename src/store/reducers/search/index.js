import {
  GET_ALL_SEARCH_RESULTS_PENDING,
  GET_ALL_SEARCH_RESULTS_SUCCESS,
  GET_ALL_SEARCH_RESULTS_FAILED,
  DELETE_SEARCH_RESULT,
  UPDATE_SEARCH_RESULT,
} from 'store/static';

export const defaultState = {
  data: [],
  error: null,
  pending: false,
};

export const searchReducer = (state = defaultState, action = {}) => {
  const { type, payload } = action;
  switch (type) {
  case GET_ALL_SEARCH_RESULTS_PENDING: {
    return { ...state, error: false, pending: true };
  }
  case GET_ALL_SEARCH_RESULTS_SUCCESS: {
    return { ...state, data: payload, error: null, pending: false };
  }
  case GET_ALL_SEARCH_RESULTS_FAILED: {
    return { ...state, error: payload, pending: false };
  }
  case DELETE_SEARCH_RESULT: {
    return { error: null, pending: false, data: state.data.filter(i => i.id !== action.payload) };
  }
  case UPDATE_SEARCH_RESULT: {
    return { error: null, pending: false, data: state.data.map(i => i.id == action.payload.id ? action.payload : i) };
  }
  default:
    return state;
  }
};

