import { searchReducer, defaultState } from './index';

import {
  GET_ALL_SEARCH_RESULTS_PENDING,
  GET_ALL_SEARCH_RESULTS_SUCCESS,
  GET_ALL_SEARCH_RESULTS_FAILED,
  DELETE_SEARCH_RESULT,
  UPDATE_SEARCH_RESULT,
} from 'store/static';

/* global describe, it, expect */

describe('Search reducer test', () => {
  it('Should return initial state', () => {
    expect(searchReducer({}, { type: null, payload: null })).toEqual({});
    expect(searchReducer(defaultState, { type: null, payload: null })).toEqual(defaultState);
  });

  it('Should set pending to true if GET_ALL_SEARCH_RESULTS_PENDING action executed', () => {
    expect(searchReducer(defaultState, { type: GET_ALL_SEARCH_RESULTS_PENDING }).pending).toEqual(true);
  });

  it('Should set pending to false and payload to data if GET_ALL_SEARCH_RESULTS_SUCCESS action executed', () => {
    const payload = [{ id: '12313', name: 'name' }, { id: '4234234', name: 'name1' }];
    const reducerResult = searchReducer(defaultState, { type: GET_ALL_SEARCH_RESULTS_SUCCESS, payload });
    expect(reducerResult.pending).toEqual(false);
    expect(reducerResult.data).toEqual(payload);
    expect(reducerResult.error).toEqual(null);
  });

  it('Should set pending to false and error to error value if GET_ALL_SEARCH_RESULTS_FAILED action executed', () => {
    const payload = Error('Some error');
    const reducerResult = searchReducer(defaultState, { type: GET_ALL_SEARCH_RESULTS_FAILED, payload });
    expect(reducerResult.pending).toEqual(false);
    expect(reducerResult.error).toEqual(payload);
    expect(reducerResult.data).toEqual(defaultState.data);
  });

  it('Should properly delete search result by id if DELETE_SEARCH_RESULT action executed', () => {
    const firstItem = { id: '12313', name: 'name' };
    const itemToDelete = { id: '4234234', name: 'name1' };
    const initialData = [firstItem, itemToDelete];

    const reducerResult = searchReducer({ ...defaultState, data: initialData }, { type: DELETE_SEARCH_RESULT, payload: itemToDelete.id });
    expect(reducerResult.data).toHaveLength(1);
    expect(reducerResult.data).toContain(firstItem);
    expect(reducerResult.data).not.toContain(itemToDelete);
  });

  it('Should properly update search result by id if UPDATE_SEARCH_RESULT action executed', () => {
    const firstItem = { id: '12313', name: 'name' };
    const secondItem = { id: '123rtr13', name: 'name00' };
    const updatedItem = { id: '123rtr13', name: 'nameNew' };
    const initialData = [firstItem, secondItem];

    const reducerResult = searchReducer({ ...defaultState, data: initialData }, { type: UPDATE_SEARCH_RESULT, payload: updatedItem });
    expect(reducerResult.data).toHaveLength(2);
    expect(reducerResult.data).toContain(firstItem);
    expect(reducerResult.data).toContain(updatedItem);
  });
});
