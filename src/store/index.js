import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';


import createRootReducer from './reducers';
import sagas from './sagas';

let store = null;

const configureStore = (initialState = {}) => {
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [sagaMiddleware];
  if (process.env.NODE_ENV === 'development') {
    const { logger } = require('redux-logger'); // eslint-disable-line global-require
    middlewares.push(logger);
  }

  const createStoreWithMiddleware = compose(
    composeWithDevTools(applyMiddleware(...middlewares))
  )(createStore);

  store = createStoreWithMiddleware(
    createRootReducer(),
    initialState
  );

  sagaMiddleware.run(sagas);
  return store;
};

const getStore = () => store;

export { getStore };
export default configureStore;
