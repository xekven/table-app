import {
  GET_ALL_SEARCH_RESULTS,
  DELETE_SEARCH_RESULT,
  UPDATE_SEARCH_RESULT,
} from 'store/static';

export const getSearchResults = () => ({ type: GET_ALL_SEARCH_RESULTS });

export const deleteSearchResult = id => ({ type: DELETE_SEARCH_RESULT, payload: id });

export const updateSearchResult = data => ({ type: UPDATE_SEARCH_RESULT, payload: data });

