import { get } from 'lodash';

export default function (error) {
  if (!error) return null;
  return get(error, 'message', 'Error occured');
}

