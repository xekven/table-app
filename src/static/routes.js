import NotFound from 'views/NotFound';
import Table from 'views/Table';

export default [
  { exact: true, component: Table, path: '/' },
  { exact: false, component: NotFound, path: '*' },
];
