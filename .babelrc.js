const aliases = require('./aliases');

module.exports = {
  plugins: [
    [
      require.resolve('babel-plugin-module-resolver'),
      {
        alias: aliases,
      }
    ],
  ],
  presets: [
    "@babel/env",
    "@babel/react"
  ]
}