# Table app

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Aliases and folder structure

Project contains resolved aliases in order to avoid relative imports (../../../something.js).

Aliases structure is as follows:

1. store - src/store - folder where state management is kept (setup of store, loggers, sagas, etc.)
1. reducers - src/store/reducers - redux reducers foldder
1. actions - src/store/actions - redux actions folder
1. utils - src/utils - folder for small utilities (timers, data mappers, etc.)
1. api - src/api - api clients 
1. app - src/app - main app folder
1. static - src/static - static content (key/values collection, static configurations)
1. hooks - src/app/hooks - folder for hooks
1. components - src/app/components - folder for components which are specific per view and cannot be reused
1. common - src/app/common - folder for common stuff across project (buttons, titles, tables, etc..)
1. views - src/app/views - actual views folder from where views are taken directly to router

## Used libraries 
1. axios - api client
1. eslint & plugins - linting
1. material table - data table (table is extremely complicated component in the and and material-table is perfect fit in terms if you need something ready to use with styles, if there would be something production-like, react-table is more suitable as it's headless and more configurable. IMO no need to re-invent wheel creating own table library if there are widely used headless ones.)
1. react-router, react-router-dom - routing
1. styled-components - CSS-IN-JS styling
1. redux - state-management
1. redux-saga - handling async action behaviout

## What can be better?
1. Testing async saga's behaviour as well as testing ui with react-testing-library
1. Replace material-table with react-table, to have more flexibility
1. Creating theming and more common components